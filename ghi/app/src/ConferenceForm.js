import React, { useEffect, useState } from "react";

export default function ConferenceForm() {
  // Fetch locations and inject into select drop down
  const [locations, setLocations] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const [conferenceName, setConferenceName] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [description, setDescription] = useState("");
  const [maxPresentations, setMaxPresentations] = useState("");
  const [maxAttendees, setMaxAttendees] = useState("");
  const [location, setLocation] = useState("");

  const handleConferenceNameChange = (event) => {
    setConferenceName(event.target.value);
  };

  const handleStarDateChange = (event) => {
    setStartDate(event.target.value);
  };

  const handleEndDateChange = (event) => {
    setEndDate(event.target.value);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleMaxPresentationChange = (event) => {
    setMaxPresentations(event.target.value);
  };

  const handleMaxAttendeeChange = (event) => {
    setMaxAttendees(event.target.value);
  };

  const handleLocationChange = (event) => {
    setLocation(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Create JSON object which takes data from POST and send to backend
    const data = {};

    data.name = conferenceName;
    data.starts = startDate;
    data.ends = endDate;
    data.description = description;
    data.max_presentations = maxPresentations;
    data.max_attendees = maxAttendees;
    data.location = location;

    // POST
    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);

    if (response.ok) {
      const newConference = await response.json();

      // Reset form to empty
      setConferenceName("");
      setStartDate("");
      setEndDate("");
      setDescription("");
      setMaxPresentations("");
      setMaxAttendees("");
      setLocation("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleConferenceNameChange}
                value={conferenceName}
                placeholder="Conference name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Conference name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleStarDateChange}
                value={startDate}
                placeholder="Start date"
                required
                type="date"
                name="starts"
                id="starts"
                className="form-control"
              />
              <label htmlFor="starts">Start date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleEndDateChange}
                value={endDate}
                placeholder="End date"
                required
                type="date"
                name="ends"
                id="ends"
                className="form-control"
              />
              <label htmlFor="ends">End date</label>
            </div>
            <div className="form-floating mb-3">
              <textarea
                onChange={handleDescriptionChange}
                value={description}
                className="form-control"
                placeholder="Description"
                required
                type="text"
                name="description"
                id="description"
              ></textarea>
              <label htmlFor="description">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleMaxPresentationChange}
                value={maxPresentations}
                placeholder="Maximum presentations"
                required
                type="number"
                name="max_presentations"
                id="max_presentations"
                className="form-control"
              />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleMaxAttendeeChange}
                value={maxAttendees}
                placeholder="Maximum attendees"
                required
                type="number"
                name="max_attendees"
                id="max_attendees"
                className="form-control"
              />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleLocationChange}
                value={location}
                required
                name="location"
                id="location"
                className="form-select"
              >
                <option value="">Choose a location</option>
                {locations.map((location) => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
