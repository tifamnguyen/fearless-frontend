import React, { useState, useEffect } from "react";

function AttendeesList(prps) {
  const [attendees, setAttendees] = useState([]);

  async function fetchAttendees() {
    const response = await fetch("http://localhost:8001/api/attendees/");

    if (response.ok) {
      const data = await response.json();
      setAttendees(data.attendees);
    }
  }

  useEffect(() => {
    fetchAttendees();
  }, []);

  return (
    <div className="container">
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Conference</th>
          </tr>
        </thead>
        <tbody>
          {attendees.map((attendee) => {
            return (
              <tr key={attendee.href}>
                <td>{attendee.name}</td>
                <td>{attendee.conference}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AttendeesList;
