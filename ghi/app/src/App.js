import Nav from "./Nav";
import MainPage from "./Main";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";
import PresentationForm from "./PresentationForm";
import { BrowserRouter, Routes, Route } from "react-router-dom";

export default function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="/locations/new" element={<LocationForm />}></Route>
        <Route path="/conferences/new" element={<ConferenceForm />}></Route>
        <Route path="/presentations/new" element={<PresentationForm />}></Route>
        <Route path="/attendees" element={<AttendeesList />}></Route>
        <Route path="/attendees/new" element={<AttendConferenceForm />}></Route>
      </Routes>
    </BrowserRouter>
  );
}
