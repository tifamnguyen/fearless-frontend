function createCard(
  title,
  location,
  description,
  pictureUrl,
  startDate,
  endDate
) {
  return `
        <div class="card shadow p-3 mb-5 bg-white rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${title}</h5>
              <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
              <p class="card-text">${description}</p>
            </div>
            <div class="card-footer text-muted">
                ${new Date(startDate).toLocaleDateString()} -
                ${new Date(endDate).toLocaleDateString()}
            </div>
        </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  const response = await fetch(url);

  if (!response.ok) {
    const alert = document.createElement("div");
    alert.innerHTML = `<div class="alert alert-danger" role="alert">
            Response NOT ok!
          </div>`;
    const mainTag = document.querySelector("main");
    mainTag.appendChild(alert);
  } else {
    const data = await response.json();

    let index = 0;
    for (let conference of data.conferences) {
      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);

      if (detailResponse.ok) {
        const details = await detailResponse.json();

        const title = details.conference.name;
        const location = details.conference.location.name;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const startDate = details.conference.starts;
        const endDate = details.conference.ends;
        const html = createCard(
          title,
          location,
          description,
          pictureUrl,
          startDate,
          endDate
        );
        const column = document.querySelector(`#col-${index % 3}`);
        column.innerHTML += html;
        index += 1;
      }
    }
  }
});
